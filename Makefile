# -*- makefile-gmake -*-
# ----------------------------------------------------------------------

PKGNAME = ctrl2hdl
AVAILABLE_LIBs = cn2hw
AVAILABLE_LIB_ITFs = cn2hw
INSTALL_LIBS = no
INSTALL_DOCS = no
ENABLE_BYTE = yes
ENABLE_NATIVE = no

-include config.mk

EXECS = ctrl2hdl

# ---

OCAMLBUILDFLAGS = -j 8
NO_PREFIX_ERROR_MSG = Missing prefix: execute configure script first

# ---

OPAM_PKGDEV_DIR ?= opam-pkgdev

OPAM_DIR = opam
OPAM_FILES = descr opam
DIST_FILES = configure LICENSE Makefile src _tags version.ml.in	\
    myocamlbuild.ml etc META.in

# ----------------------------------------------------------------------

-include generic.mk

GENERIC_MK = $(OPAM_PKGDEV_DIR)/generic.mk
generic.mk:
	@if test -f $(GENERIC_MK); then ln -s $(GENERIC_MK) $@;		\
	 elif test \! -f generic.mk; then echo				\
"To build from this development tree, you first need to retrieve the"	\
"$(OPAM_PKGDEV_DIR) submodule using \`git submodule update'."		\
	 >/dev/stderr; exit 1; fi;

# ----------------------------------------------------------------------

.PHONY: distclean
distclean: force clean clean-version
	$(QUIET)rm -f config.mk

# ----------------------------------------------------------------------
