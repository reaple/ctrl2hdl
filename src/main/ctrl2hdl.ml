open Format
open Filename
open Rutils
open IO
open Processing


(* -------------------------------------------------------------------------- *)
(* Options: *)

(** File extensions officially understood, with associated input types. *)
let ityps_alist = [
  "ctrlf", `Ctrlf; "cf", `Ctrlf;
  "ctrln", `Ctrln; "cn", `Ctrln;
  "ctrld", `Ctrln; "cd", `Ctrln;
]

(** Name of official input types as understood by the tool. *)
let ityps = List.map fst ityps_alist

let set_input_type r t =
  try r := Some (List.assoc t ityps_alist) with
    | Not_found -> raise (Arg.Bad (asprintf "Unknown input file type: `%s'" t))

(* --- *)

(** File extensions officially understood, with associated output types. *)
let otyps_alist = [
  "v", `Verilog; "verilog", `Verilog; "vhdl", `Vhdl;
]

(** Name of official output types as understood by the tool. *)
let otyps = List.map fst otyps_alist

let default_output_type = `Verilog
let set_output_type r t =
  try r := Some (List.assoc t otyps_alist) with
    | Not_found -> raise (Arg.Bad (asprintf "Unknown output file type: `%s'" t))

(* --- *)

module WarningFilters = struct
  let silence_ignored_variables = ref false
end
open WarningFilters

let inputs = ref []
let output = ref ""
let module_name = ref ""
let clock_name = ref "clock"
let reset_name = ref "reset"
let assertion_signal = ref ""
let implicit_inits = ref false
let input_type: [ `Ctrlf | `Ctrln ] option ref = ref None
let output_type: [ `Verilog | `Vhdl ] option ref = ref None
let propagate_constants = ref true

exception Help
let usage = "Usage: ctrl2hdl [options] [ -- ] { <ctrlf> | <ctrln> }"
let print_vers () =
  fprintf err_formatter "ctrl2hdl-%s (Compiled on %s, %s)@." Version.str
    Version.compile_host Version.compile_time;
  exit 0
let anon x = inputs := x :: !inputs
let u = Arg.Unit (fun _ -> ())
let e s = ("", u, " "^s)
let t s = ("", u, " \ro __"^s^"__")
let h = Arg.Unit (fun _ -> raise Help)
let v = Arg.Unit print_vers
let options = Arg.align
  [
    e""; t"Input/Output"; e"";

    ("-i", Arg.String anon, "<file> ");
    ("--input", Arg.String anon, "<file> Input file");
    ("--input-type", Arg.Symbol (ityps, set_input_type input_type),
     " Input file type");
    ("-o", Arg.Set_string output, "<file> ");
    ("--output", Arg.Set_string output,
     "<file> Main output file (`-' means standard output)");
    ("--output-type", Arg.Symbol (otyps, set_output_type output_type),
     " Output file type");
    ("--", Arg.Rest anon, " Treat all remaining arguments as input files");

    e""; t"Translation"; e"";

    ("--module-name", Arg.Set_string module_name,
     "<module> Set the name of the generated module (guessed from");
    e" input file name by default)";
    ("--assert-signal", Arg.Set_string assertion_signal,
     "<signal> Enable and name an additional 1-wire output signal");
    e" encoding the assertion";
    ("--clock-name", Arg.Set_string clock_name,
     "<signal> Set the name of the clock signal");
    e(sprintf " (\"%s\" by default)" !clock_name);
    ("--reset-name", Arg.Set_string reset_name,
     "<signal> Set the name of the synchronous reset signal");
    e(sprintf " (\"%s\" by default)" !reset_name);
    ("--implicit-inits", Arg.Set implicit_inits, " \
       Use implicit values for register initializations");
    e" whenever possible";

    e""; t"Warning filters"; e"";

    ("-Wignored-variables", Arg.Set silence_ignored_variables,
     " Silence warnings about ignored variables");

    e""; t"Optimizations"; e"";

    ("--no-constant-propagation", Arg.Clear propagate_constants,
     " Disable constant propagation");

    e""; t"Miscellaneous"; e"";

    ("-V", v, " ");
    ("--version", v, " Display version info");
    ("-h", h, " ");
    ("-help", h, " ");
    ("--help", h, " Display this list of options");
  ]

let arror f = error ~cont:(fun () -> Arg.usage options usage; exit 1) f

(* -------------------------------------------------------------------------- *)

module type PROC = sig
  module E: Cn2hw.ERRORS
  module GenParams: Cn2hw.GEN_PARAMS with type flg = CtrlNbac.Loc.t
  val process: ('a -> 'b) -> ?filename: string -> bool -> 'a -> bool * 'b
end

module Processor (F: Cn2hw.FILTERS): PROC = struct
  module E = Cn2hw.Errors (F)

  let es = E.make ()

  module GenParams = struct
    module Errors = E
    type flg = CtrlNbac.Loc.t
    let push_err = E.push es
    let push_wrn = E.pushw es
    let exit_on_errors () = if E.chk_errors es then raise Exit
  end

  let report_msgs ?filename () =
    E.sort CtrlNbac.Loc.compare es;              (* Sort messages by location *)
    E.iterw es (fun (loc, e) ->
      CtrlNbac.Parser.Reporting.warning ?filename ?loc
        (fun fmt -> Cn2hw.pp_warning fmt e) err_formatter);
    E.iter es (fun (loc, e) ->
      CtrlNbac.Parser.Reporting.error ?filename ?loc
        (fun fmt -> Cn2hw.pp_error fmt e) err_formatter)

  let process f ?filename err e =
    try
      E.reset es;
      let pp = f e in
      report_msgs ?filename ();
      err || E.chk_errors es, pp
    with
      | Exit -> report_msgs ?filename (); exit 1
end

(* -------------------------------------------------------------------------- *)

open Hardcaml

module type COMB = Comb.S with type t = Signal.t

module Basic: COMB = Signal
module ConstProp: COMB = struct
  include Signal.Const_prop.Comb
  type t = Signal.t
end

module MakeParams () = struct
  let clock_name = match !clock_name with "" -> "clock" | s -> s
  let clock = Signal.input clock_name 1
  let reset_name = match !reset_name with "" -> "reset" | s -> s
  let reset = Signal.input reset_name 1
  let assertion_signal = match !assertion_signal with "" -> None | s -> Some s
end

let comb_circuit (module P: PROC) ?filename func_name func err : Circuit.t =
  let open P in
  let module Comb: COMB = (val match !propagate_constants with
    | false -> (module Basic: COMB)
    | true -> (module ConstProp))
  in
  let module T = Cn2hw.Make (GenParams) (Comb) (MakeParams ()) in
  let _err, (_te, outs) = process T.func ?filename false func in
  Circuit.create_exn
    ~name:func_name
    ~detect_combinational_loops:true
    ~normalize_uids:true
    outs

let seq_circuit (module P: PROC) ?filename node_name node err : Circuit.t =
  let open P in
  let module Comb: COMB = (val match !propagate_constants with
    | false -> (module Basic: COMB)
    | true -> (module ConstProp))
  in
  let module T = Cn2hw.Make (GenParams) (Comb) (MakeParams ()) in
  let use_implicit_inits = !implicit_inits in
  let _err, (_te, outs) =
    process (T.node ~use_implicit_inits) ?filename false node in
  Circuit.create_exn
    ~name:node_name
    ~detect_combinational_loops:true
    ~normalize_uids:true
    outs

(* -------------------------------------------------------------------------- *)

let output_circuit ot mk_oc c = match ot with
  | `Verilog -> output_verilog mk_oc c
  | `Vhdl -> output_vhdl mk_oc c

let handle_ctrlf (module P: PROC) ?filename (ot, mk_oc) =
  let err = false in
  let func_name, func = parse_input ?filename CtrlNbac.Parser.parse_func in
  let func_name = match !module_name, func_name with
    | m, _ when m <> "" -> m
    | _, Some f -> f
    | _, None -> "function"
  in
  comb_circuit (module P) ?filename func_name func err |>
      output_circuit ot mk_oc

let handle_ctrln (module P: PROC) ?filename (ot, mk_oc) =
  let err = false in
  let node_name, node = parse_input ?filename CtrlNbac.Parser.parse_node in
  let node_name = match !module_name, node_name with
    | m, _ when m <> "" -> m
    | _, Some f -> f
    | _, None -> "node"
  in
  seq_circuit (module P) ?filename node_name node err |>
      output_circuit ot mk_oc

(* -------------------------------------------------------------------------- *)

let ityp_name_n_handle = function
  | `Ctrlf | `Cf _ -> "function", handle_ctrlf
  | `Ctrln | `Cn _ -> "node", handle_ctrln

let ityp_name x = ityp_name_n_handle x |> fst

let guessityp f =
  try match !input_type with
    | Some t -> t
    | None -> snd (List.find (fun (s, _) -> check_suffix f s) ityps_alist)
  with
    | Not_found ->
        raise (Arg.Bad (sprintf "Cannot guess input type of `%s'" f))

(* let guessityp' f = match guessityp f with *)
(*   | `Ctrlf -> `Cf f *)
(*   | `Ctrln -> `Cn f *)

let guessout f = match !output with
  | "-" -> mk_std_oc
  | "" -> (try chop_extension f |> mk_oc with
      | Invalid_argument _ when f <> "" -> mk_oc f
      | Invalid_argument _ -> mk_std_oc)
  | o -> mk_oc' o

let guess_output_channel () = match !output with
  | "-" | "" -> mk_std_oc
  | o -> mk_oc' o

(* --- *)

let guessotyp f =
  try match !output_type with
    | Some t -> t
    | None -> snd (List.find (fun (s, _) -> check_suffix f s) otyps_alist)
  with
    | Not_found ->
        raise (Arg.Bad (sprintf "Cannot guess output type for `%s'" f))

let guess_output_type () = match !output_type, !output with
  | Some o, _ -> o
  | None, ("-" | "") -> default_output_type
  | None, f -> guessotyp f

(* --- *)

let handle_input_file (module P: PROC) ?ityp filename =
  let ityp = match ityp with
    | None -> guessityp filename
    | Some ityp -> ityp
  in
  let mk_oc = guessout filename and otyp = guess_output_type () in
  let itypname, handle = ityp_name_n_handle ityp in
  Rutils.Log.i logger "@[Reading@ %s@ from@ `%s'…@]" itypname filename;
  handle (module P) ~filename (otyp, mk_oc);
  reset ()

let handle_input_stream (module P: PROC) = function
  | None ->
      Log.i logger "@[Reading@ function@ from@ standard@ input…@]";
      handle_ctrlf (module P) (guess_output_type (), guess_output_channel ())
  | Some ityp ->
      let itypname, handle = ityp_name_n_handle ityp in
      Log.i logger "@[Reading@ %s@ from@ standard@ input…@]" itypname;
      handle (module P) (guess_output_type (), guess_output_channel ())

let distinguish_n_handle_inputs (module P: PROC) = function
  | [] -> handle_input_stream (module P) !input_type
  | lst -> List.iter (handle_input_file (module P) ?ityp:!input_type) lst

let main () =
  Arg.parse options anon usage;
  Log.i logger "@[This@ is@ ctrl2hdl@ version@ %s.@]" Version.str;
  let module P = (Processor (struct
    let silence_ignored_variables = !silence_ignored_variables
  end) : PROC) in
  distinguish_n_handle_inputs (module P) (List.rev !inputs)

(* -------------------------------------------------------------------------- *)

let _ = try main () with
  | Help -> Arg.usage options usage; exit 0
  (* | Ctrl2But.UnsupportedWeavedNode -> error "Unsupported: weaved@ node." *)
  | Failure s | Sys_error s -> error "%s" s
  | Exit -> abort "" []

(* -------------------------------------------------------------------------- *)
