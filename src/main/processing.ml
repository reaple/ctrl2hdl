open Format
open Rutils.IO
open CtrlNbac.IO

let pp = fprintf

let error ?cont f =
  let maybecont fmt =
    pp_print_newline fmt ();
    match cont with None -> exit 1 | Some c -> c ()
  in
  pp err_formatter "Error: @[";
  kfprintf maybecont err_formatter f
let warn f =
  pp err_formatter "Warning: @[";
  kfprintf (fun fmt -> pp_print_newline fmt ()) err_formatter f
let info f =
  pp err_formatter "Info: @[";
  kfprintf (fun fmt -> pp_print_newline fmt ()) err_formatter f

let abort ?filename n msgs =
  CtrlNbac.IO.report_msgs ?filename err_formatter msgs;
  error "Aborting due to errors in %(%)" n

(* -------------------------------------------------------------------------- *)

let parse_input ?filename p = parse_input ~abort ?filename p

(* -------------------------------------------------------------------------- *)

(* XXX: check errors ahead of printting the header in the two following
   functions! Maybe use a buffer… *)

let output_verilog =
  mk_output'
    ~ext:"v"
    ~out_descr:"Verilog@ circuit"
    ~version:Version.str
    ~print:(fun ?print_header oc ->
      begin match print_header with
        | Some p -> p oc "/*" "*/"
        | None -> ()
      end;
      Hardcaml.Rtl.output
        ~output_mode:(Hardcaml.Rtl.Output_mode.To_channel oc)
        Hardcaml.Rtl.Language.Verilog)

let output_vhdl mk_oc =
  mk_output
    ~ext:"vhdl"
    ~out_descr:"VHDL@ circuit"
    ~version:Version.str
    ~print:(fun ?print_header fmt c ->
      begin match print_header with
        | Some p ->
            let { out_string; out_newline } as ofuncs =
              pp_get_formatter_out_functions fmt () in
            pp_set_formatter_out_functions fmt
              { ofuncs with
                out_newline = (fun () -> out_string "\n--" 0 3); };
            p fmt "--" "";
            pp_set_formatter_out_functions fmt ofuncs;
            pp_print_newline fmt ();
            pp_print_newline fmt ()
        | None -> ()
      end;
      let buff = Buffer.create 1024 in
      Hardcaml.Rtl.output
        ~output_mode:(Hardcaml.Rtl.Output_mode.To_buffer buff)
        Hardcaml.Rtl.Language.Vhdl c;
      pp_print_as fmt 0 (Buffer.contents buff))
    (mk_fmt_of_oc mk_oc)

(* -------------------------------------------------------------------------- *)

let straighten_if_tty oc fo cols =
  if Unix.isatty (Unix.descr_of_out_channel oc) then
    pp_set_margin fo cols

let _ =
  let columns = try int_of_string (Sys.getenv "COLUMNS") with _ -> 80 in
  straighten_if_tty stderr err_formatter columns;
  straighten_if_tty stdout std_formatter columns;
  CtrlNbac.Symb.reset ()

let reset = CtrlNbac.Symb.reset
