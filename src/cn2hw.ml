open Format
module Symb = CtrlNbac.Symb
open CtrlNbac.AST

(* -------------------------------------------------------------------------- *)

(** Potentially localized error messages returned by functions below. *)
type 'f error =
  | EExp of 'f exp
  | EPoly of 'f exp
  | ETVar of typ * string * symb
  | ECmpSign of totrel
  | ENopSign of nnop
  | EBadConj

let pp_error fmt = let pp = fprintf in function
  | EExp e ->
      pp fmt "Unsupported@ type@ of@ expression:@ `%a'" print_exp e
  | EPoly e ->
      pp fmt "Unsupported@ untyped@ expression:@ `%a'" print_exp e
  | ETVar (t, s, v) ->
      pp fmt "Unsupported@ %s@ variable@ `%a'@ of@ type@ %a\
             " s Symb.print v print_typ t
  | ECmpSign _ ->
      pp fmt "Comparison@ between@ signed@ and@ unsigned@ values"
  | ENopSign _ ->
      pp fmt "Operation@ between@ signed@ and@ unsigned@ values"
  | EBadConj ->
      pp fmt "Conjunction@ of@ distinct@ equalities@ with@ constants@ \
              expected"

(* --- *)

(** Potentially localized warning messages *)
type 'f warning =
  | WIgnVarOfTyp of typ * string * symb

let pp_warning fmt = let pp = fprintf in function
  | WIgnVarOfTyp (t, s, v) ->
      pp fmt "Ignoring@ %s@ variable@ `%a'@ of@ type@ %a\
             " s Symb.print v print_typ t

(* --- *)

module type ERRORS = sig
  type 'f t
  val make: unit -> 'f t
  val reset: 'f t -> unit
  val chk_errors: 'f t -> bool
  val sort: ('f -> 'f -> int) -> 'f t -> unit
  val push: 'f t -> 'f option * 'f error -> unit
  val iter: 'f t -> ('f option * 'f error -> unit) -> unit
  val pushw: 'f t -> 'f option * 'f warning -> unit
  val iterw: 'f t -> ('f option * 'f warning -> unit) -> unit
end

module type FILTERS = sig
  val silence_ignored_variables: bool
end

module Errors (F: FILTERS) : ERRORS = struct
  open F

  type 'f t =
      {
        mutable err_msgs: ('f option * 'f error) list;
        mutable wrn_msgs: ('f option * 'f warning) list;
      }

  let make () = { err_msgs = []; wrn_msgs = [] }
  let reset es = es.err_msgs <- []; es.wrn_msgs <- []
  let chk_errors { err_msgs } = err_msgs <> []
  let sort' cmp_flags =
    List.sort (fun (al, _) (bl, _) -> match al, bl with
      | Some a, Some b -> cmp_flags a b
      | _ -> 0)
  let sort cmp_flags es =
    es.err_msgs <- sort' cmp_flags es.err_msgs;
    es.wrn_msgs <- sort' cmp_flags es.wrn_msgs
  let push es e = es.err_msgs <- e :: es.err_msgs
  let iter { err_msgs } f = List.iter f err_msgs
  let pushw es = function
    | _, WIgnVarOfTyp _ when silence_ignored_variables -> ()
    | w -> es.wrn_msgs <- w :: es.wrn_msgs
  let iterw { wrn_msgs } f = List.iter f wrn_msgs
end

(* -------------------------------------------------------------------------- *)

module HC = Hardcaml

module type GEN_PARAMS = sig
  module Errors: ERRORS
  type flg
  val push_err: flg option * flg error -> unit
  val push_wrn: flg option * flg warning -> unit
  val exit_on_errors: unit -> unit
end

module type INPUT_PARAMS = sig
  type signal
  val clock: signal
  val reset: signal
  val assertion_signal: string option
end

module type S = sig
  type flg
  type typenv
  type signal = HC.Signal.t

  val setup_typenv
    : flg typdefs
    -> typenv

  val node
    : ?typenv: typenv
    -> ?use_implicit_inits: bool
    -> flg checked_node
    -> typenv * signal list

  val func
    : ?typenv: typenv
    -> flg checked_func
    -> typenv * signal list
end

module Make
  (GenP: GEN_PARAMS)
  (Comb: HC.Comb.S with type t = HC.Signal.t)
  (P: INPUT_PARAMS with type signal := Comb.t)
  :
  (S with type flg = GenP.flg)
  =
struct

  open HC.Signal
  open Comb
  type flg = GenP.flg
  type signal = Comb.t
  type enum = signal
  type bint = [ `Bint of bool * width ] * signal

  open GenP
  open Errors
  open P

  let err f e d = push_err (f, e); d
  let wrn f e d = push_wrn (f, e); d

  let id x = x

  (* --- *)

  let register name w =
    let d = wire w and reset_to = wire w in
    let rtype = HC.Reg_spec.create ~clock ~reset () in
    let rtype = HC.Reg_spec.override ~reset_to rtype in
    d, wireof (reg rtype ~enable:empty d) -- name, reset_to

  (* --- *)

  let min_width s i =
    if i = 0 then 0 else
      let sign = i < 0 in
      let rec cont acc w =
        if (not sign && acc <= i) || (sign && acc < -i)
        then cont (acc lsl 1) (w + 1)
        else if sign then w + 1 else w
      in
      cont 1 0 + if s && not sign then 1 else 0

  (* --- *)

  type typenv = signal SMap.t * int SMap.t
  type gen_data =
      {
        signals: signal SMap.t;
        ntyp: [ `Bint of bool * width ] SMap.t;
        etyp_width: int SMap.t;
      }

  (* --- *)

  let sign: bint -> bool = fun (`Bint (s, _), _) -> s
  let ntyp: bint -> [ `Bint of bool * width ] = fst
  let signal: bint -> signal = snd

  let resize s w e = (if s then sresize else uresize) e w

  let apply1: (signal -> signal) -> bint -> bint
    = fun f (t, a) -> (t, f a)

  let apply2: (signal -> signal -> signal) -> bint -> bint -> bint
    = fun f (`Bint (sa, wa) as ta, a) (`Bint (sb, wb), b) ->
      assert (sa == sb);                          (* signs should always match *)
      let t, (a, b) =                            (* auto-expansion *)
        if wa = wb then (ta, (a, b)) else
          let s = sa and w = max wa wb in
          let t = `Bint (s, w) in
          (t, if w = wa then (a, resize s w b) else (resize s w a, b))
      in
      (t, f a b)

  let teqrel = function
    | `Eq -> (==:)
    | `Ne -> (<>:)

  let ttotrel ?flag o a b : signal =
    let a, sa, b, sb = signal a, sign a, signal b, sign b in
    match o, sa, sb with
      | #eqrel as r, _, _ -> (teqrel r) a b
      | `Lt, false, false -> a <: b
      | `Lt, true, true   -> a <+ b
      | `Le, false, false -> a <=: b
      | `Le, true, true   -> a <=+ b
      | `Gt, false, false -> a >: b
      | `Gt, true, true   -> a >+ b
      | `Ge, false, false -> a >=: b
      | `Ge, true, true   -> a >=+ b
      | _ -> err flag (ECmpSign o) vdd

  let tnuop ?flag : nuop -> bint -> bint = function
    | `Opp -> apply1 negate

  let tluop ?flag : luop -> bint -> bint = function
    | `LNot -> apply1 (~:)

  let tlbop ?flag : lbop -> bint -> bint -> bint = function
    | `LAnd -> apply2 (&:)
    | `LOr -> apply2 (|:)
    | `LXor -> apply2 (^:)

  let tlsop ?flag : lsop -> bint -> bint -> bint = fun o a b ->
    let sa, sb = sign a, sign b and ta = ntyp a in
    let b = signal b and a = signal a in
    if not sb then
      ta, match o with
        | (`Lsl | `Asl) -> log_shift sll a b                (* _ << u, _ <<< u *)
        | `Lsr -> log_shift srl a b                         (* _ >> u *)
        | `Asr when sa -> log_shift sra a b                 (* s >>> u *)
        | `Asr -> log_shift srl a b                         (* u >>> u *)
    else
      let bneg = msb b and b' = negate b in
      ta, match o with
        | `Lsl -> mux2 bneg (log_shift srl a b') (log_shift sll a b) (* _ << s *)
        | `Asl -> mux2 bneg (log_shift sra a b') (log_shift sll a b) (* _ <<< s *)
        | `Lsr -> mux2 bneg (log_shift sll a b') (log_shift srl a b) (* _ >> s *)
        | `Asr -> mux2 bneg (log_shift sll a b') (log_shift sra a b) (* _ >>> s *)

  let tnnop ?flag o a b : bint = match o, sign a, sign b with
    | `Sum, _, _ -> apply2 (+:) a b
    | `Sub, _, _ -> apply2 (-:) a b
    | `Mul, false, false -> apply2 ( *: ) a b
    | `Mul, true,  true  -> apply2 ( *+ ) a b
    | `Div, _, _ -> raise Exit
    | _ -> err flag (ENopSign o) a

  let op_mapfold o = List.fold_left o
  let op_bmapfold = function
    | `Conj -> op_mapfold (&:)
    | `Disj -> op_mapfold (|:)
    | `Excl -> assert false

  (* --- *)

  let deref { signals } ?flag v = try SMap.find v signals with
    | Not_found -> eprintf "Signal not found: %a@." Symb.print v; raise Exit

  let derefn ({ ntyp } as gd) ?flag v =
    SMap.find v ntyp, deref gd ?flag v

  let nm ?name w = match name with
    | None -> w
    | Some n -> w -- n

  let nm2 ?name (s, w) = s, match name with
    | None -> w
    | Some n -> w -- n

  let app1s o (sa, a) = o (sa, a)
  let biite cond (sa, a) (sb, b) = if sa <> sb then raise Exit; sa, (cond a b)
  let app2s o a b = o a b
  let appXs o = List.fold_left o

  let reflag ?flag e = match flag with
    | None -> e
    | Some f -> CtrlNbac.AST.flag f e

  let eerr d flag e = err flag e d

  let member tp e f l =
    (* XXX: use `pmuxl' or `matches' for constant f & l (???) *)
    let eqe = let e = tp e in fun x -> e ==: tp x in
    List.fold_left (fun acc x -> eqe x |: acc) (eqe f) l

  let rec tb gd ?name ?flag : 'f CtrlNbac.AST.bexp -> signal =
    let tn = tn gd and te = te gd in
    let eexp_bexp flag e = eerr vdd flag (EExp (`Bexp (reflag ?flag e))) in
    let epoly_bexp flag e = eerr vdd flag (EPoly (`Bexp (reflag ?flag e))) in
    let rec tb ?flag : 'f bexp -> signal = fun e -> match e with
      | `Ref s -> deref gd ?flag s
      | `Bool true -> vdd
      | `Bool false -> gnd
      | `Buop (`Neg, e) -> ~: (tb e)
      | `Bbop (`Imp, e, f) -> (~: (tb e)) |: (tb f)
      | `Bnop (o, e, f, l) -> op_bmapfold o (tb e) (List.map tb (f::l))
      | `Bcmp (o, e, f) -> (teqrel o) (tb e) (tb f)
      | `Ecmp (o, e, f) -> (teqrel o) (te e) (te f)
      | `Ncmp (o, e, f) -> ttotrel ?flag o (tn e) (tn f)
      | `Bin (e, f, l) -> member tb e f l
      | `Ein (e, f, l) -> member te e f l
      | `BIin (e, f, l) -> member (fun x -> tn e |> snd) e f l
      | `Ite (c, t, e) -> mux2 (tb c) (tb t) (tb e)
      | #flag as f -> apply' tb f
      | `Pcmp _ as e -> epoly_bexp flag e
      | `Pin _ -> eexp_bexp flag e
      | exception Exit -> eexp_bexp flag e
    in
    let rec tb' ?name ?flag = function
      | `Ref _ as e -> tb ?flag e
      | #flag as f -> apply' (tb' ?name) f
      | e -> tb ?flag e |> nm ?name
    in
    tb' ?name ?flag
  and te gd ?name ?flag : 'f CtrlNbac.AST.eexp -> enum =
    let tb = tb gd in
    let eerr = eerr vdd in
    let eexp_eexp flag e = eerr flag (EExp (`Eexp (reflag ?flag e))) in
    let rec te ?flag : 'f eexp -> enum = fun e -> match e with
      | `Ref s -> deref gd ?flag s
      | `Enum l -> deref gd ?flag (label_symb l)
      | `Ite (c, t, e) -> mux2 (tb c) (te t) (te e)
      | #flag as f -> apply' te f
      | exception Exit -> eexp_eexp flag e
    in
    let rec te' ?name ?flag = function
      | `Ref _ as e -> te ?flag e
      | #flag as f -> apply' (te' ?name) f
      | e -> te ?flag e |> nm ?name
    in
    te' ?name ?flag
  and tn gd ?name ?flag : 'f CtrlNbac.AST.nexp -> bint =
    let tb = tb gd in
    let eerr = eerr (`Bint (true, 1), vdd) in
    let eexp_nexp flag e = eerr flag (EExp (`Nexp (reflag ?flag e))) in
    let rec tn ?flag : 'f nexp -> bint = fun e -> match e with
      | `Ref s -> derefn gd ?flag s
      | `Int i ->
          let s = i < 0 in
          let w = max 1 (min_width s i) in
          `Bint (s, w), consti w i
      | `Ncst (s, w, e) ->
          let `Bint (es, _), e = tn e in
          `Bint (s, w), resize es w e
      | `Nuop (o, e) -> app1s (tnuop ?flag o) (tn e)
      | `Luop (o, e) -> app1s (tluop ?flag o) (tn e)
      | `Lbop (o, e, f) -> app2s (tlbop ?flag o) (tn e) (tn f)
      | `Lsop (o, e, s) -> app2s (tlsop ?flag o) (tn e) (tn s)
      | `Nnop (o, e, f, l) -> appXs (tnnop ?flag o) (tn e) (List.map tn (f::l))
      | `Ite (c, t, x) ->
          (try biite (mux2 (tb c)) (tn t) (tn x) with Exit -> eexp_nexp flag e)
      | #flag as f -> apply' tn f
      | `Real _ | `Mpq _ -> eexp_nexp flag e
      | exception Exit -> eexp_nexp flag e
    in
    let rec tn' ?name ?flag = function
      | `Ref _ as e -> tn ?flag e
      | #flag as f -> apply' (tn' ?name) f
      | e -> tn ?flag e |> nm2 ?name
    in
    tn' ?name ?flag
  and texp gd ?name ?flag : 'f CtrlNbac.AST.exp -> signal = function
    | `Bexp e -> tb gd ?name ?flag e
    | `Nexp e -> tn gd ?name ?flag e |> snd
    | `Eexp e -> te gd ?name ?flag e
    | (`Ref _ | `Ite _) as e -> eerr vdd flag (EPoly (reflag ?flag e))
    | #flag as f -> apply' (texp ?name gd) f

  (* --- *)

  (* XXX: assuming internaly fixed log encoding: *)
  let empty_typenv: typenv = SMap.empty, SMap.empty
  let init_labels: 'f typdefs -> typenv -> typenv =
    fold_typdefs begin fun tn (EnumDef labels, _) (lmap, etyp_width) ->
      let w = max 1 (min_width false (List.length labels - 1)) in
      let _, lmap = List.fold_left (fun (i, acc) (l, _) ->
        let l = label_symb l in
        i+1, SMap.add l (consti w i -- Symb.to_string l) acc
      ) (0, lmap) labels in
      lmap, SMap.add tn w etyp_width
    end

  let typ_width gd = function
    | `Bool -> 1
    | `Bint (_, w) -> w
    | `Enum tn -> SMap.find tn gd.etyp_width

  let register_ntyp v = function
    | `Bint _ as t -> fun gd -> { gd with ntyp = SMap.add v t gd.ntyp }
    | _ -> id

  let add_input v t ?loc gd = match t with
    | `Bool | #etyp | `Bint _ as t ->
        let w = input (Symb.to_string v) (typ_width gd t) in
        register_ntyp v t { gd with signals = SMap.add v w gd.signals }
    | t -> wrn loc (WIgnVarOfTyp (t, "input", v)) gd

  let add_register v t ?loc ((gd, ins) as acc) = match t with
    | `Bool | #etyp | `Bint _ as t ->
        let d, q, i = register (Symb.to_string v) (typ_width gd t) in
        register_ntyp v t { gd with signals = SMap.add v q gd.signals },
        SMap.add v (d, i) ins
    | t -> wrn loc (WIgnVarOfTyp (t, "state", v)) acc

  let add_local v t e ?loc gd =
    let signals gd = SMap.add v (texp ~name:(Symb.to_string v) gd e) gd.signals in
    match t with
      | `Bool | #etyp
        -> { gd with signals = signals gd }
      | `Bint (s, w) as t
        -> { gd with signals = signals gd; ntyp = SMap.add v t gd.ntyp }
      | t
        -> wrn loc (WIgnVarOfTyp (t, "local", v)) gd

  let output_signal gd v _t e =
    let name = Symb.to_string v in
    let rec aux (e: _ exp) = match deflag e with
      | `Ref _ -> texp ~name gd e
      | `Bexp e -> auxb e
      | `Nexp e -> auxn e
      | `Eexp e -> auxe e
      | _ -> SMap.find v gd.signals
    and auxb e = match deflag e with
      | `Ref _ -> texp ~name gd (`Bexp e)
      | _ -> SMap.find v gd.signals
    and auxn e = match deflag e with
      | `Ref _ -> texp ~name gd (`Nexp e)
      | _ -> SMap.find v gd.signals
    and auxe e = match deflag e with
      | `Ref _ -> texp ~name gd (`Eexp e)
      | _ -> SMap.find v gd.signals
    in
    output name (aux e)

  (* --- *)

  let setup_typenv: flg typdefs -> typenv = fun td ->
    init_labels td empty_typenv

  (* --- *)

  module Eval = CtrlNbac.Eval.Make (GenP)

  exception NonDeterministicNode

  let implicit_init gd = function
    | `Bool -> gnd
    | `Bint (s, w) -> tn gd (`Ncst (s, w, `Int 0)) |> snd
    | `Enum tn -> raise NonDeterministicNode

  let node
      ?typenv
      ?(use_implicit_inits = false)
      (node: 'f checked_node)
      =
    let { cn_typs; cn_init } as nd = node_desc node in
    let local_bindings = sorted_node_definitions node in

    (* Labels *)
    let tx = match typenv with Some te -> te | None -> setup_typenv cn_typs in
    let gd = { signals = fst tx; etyp_width = snd tx; ntyp = SMap.empty } in

    (* Initialization *)
    let init_env =
      let env = Eval.bind (fun v -> let t, _, _ = SMap.find v nd.cn_decls in t)
        Eval.Env.empty local_bindings in
      Eval.const_assignments cn_typs (label_typ (`Tds cn_typs)) env cn_init
    in
    exit_on_errors ();

    (* Inputs & Registers *)
    let gd, reg_di = SMap.fold begin fun v -> function
      | t, `Input _, loc
      | t, `Contr _, loc -> fun (gd, ri) -> add_input v t ?loc gd, ri
      | t, `State _, loc -> add_register v t ?loc
      | _ -> id
    end nd.cn_decls (gd, SMap.empty) in
    exit_on_errors ();

    (* Locals *)
    let gd = List.fold_left begin fun gd (v, e) ->
      let t, _, loc = SMap.find v nd.cn_decls in add_local v t e ?loc gd
    end gd local_bindings in
    exit_on_errors ();

    (* Registers' inputs *)
    let outs = SMap.fold begin fun v -> function
      | (`Bool | #etyp | `Bint _) as k, `State (e, _), _loc ->
          let d, i = SMap.find v reg_di in
          d <== texp gd e;
          i <== begin match Eval.Env.find_typed v init_env with
            | None when not use_implicit_inits -> raise NonDeterministicNode
            | None -> implicit_init gd k
            | Some (Eval.Val.Bool b) -> tb gd (`Bool b)
            | Some (Eval.Val.Enum e) -> te gd (`Enum e)
            | Some (Eval.Val.Bint (s, w, i)) ->
                tn gd (`Ncst (s, w, `Int (Mpz.get_int i))) |> snd
            | Some _ -> failwith "unsupported"
          end;
          List.cons (SMap.find v gd.signals)
      | _ -> id
    end nd.cn_decls [] in

    (* Assertion *)
    let outs = match assertion_signal with
      | None -> outs
      | Some name -> output name (texp ~name gd (`Bexp nd.cn_assertion)) :: outs
    in

    tx, outs

  (* --- *)

  let func ?typenv (func: 'f checked_func) =
    let fd = func_desc func in

    (* Labels *)
    let te = match typenv with Some te -> te | None -> setup_typenv fd.fn_typs in
    let gd = { signals = fst te; etyp_width = snd te; ntyp = SMap.empty } in

    (* Inputs *)
    let gd = SMap.fold begin fun v -> function
      | t, `Input _, loc -> add_input v t ?loc
      | _ -> id
    end fd.fn_decls gd in
    exit_on_errors ();

    (* Locals *)
    let gd = List.fold_left begin fun gd (v, e) ->
      let t, _, loc = SMap.find v fd.fn_decls in add_local v t e ?loc gd
    end gd (sorted_func_definitions func) in
    exit_on_errors ();

    (* Outputs *)
    let outs = SMap.fold begin fun v -> function
      | t, `Output (_, e, _), _loc -> List.cons (output_signal gd v t e)
      | _ -> id
    end fd.fn_decls [] in

    (* Assertion *)
    let outs = match assertion_signal with
      | None -> outs
      | Some name -> output name (texp ~name gd (`Bexp fd.fn_assertion)) :: outs
    in

    te, outs

end

(* -------------------------------------------------------------------------- *)
